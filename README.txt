CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers


Introduction
------------

This module add a new Simple OAuth grant type to integrate with
Social Auth Facebook.

* For a full description of this module, visit the project page:
  https://www.drupal.org/project/simple_oauth_facebook_connect

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/simple_oauth_facebook_connect


Requirements
------------

This module requires the following core:

Drupal core ^8 || ^9


Recommended Modules
-------------------

This module requires no modules outside of Drupal core.


Installation
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
  for further information.

* Optionally you can also install using composer:
  composer require 'drupal/simple_oauth_facebook_connect:1.0.x-dev@dev'


Configuration of code examples for frontend part
-------------------------------------------------

1. Create a FB App. Follow this below link:
   https://magefan.com/blog/create-facebook-application.
2. Create new html page, insert code below:

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : <INPUT_FB_APP_ID_HERE>,
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v8.0'
    });
  };
</script>
<script async defer crossorigin="anonymous"
src="https://connect.facebook.net/en_US/sdk.js">
</script>

<button id="login">LOGIN</button>
<script type="text/javascript">
  document.getElementById('login').addEventListener('click', function () {
    FB.login(function (response) {
      if (response.status == 'connected') {
        console.log(response);
      }
    })
  })
</script>

3. Run page on browser, after that Click login button.
4. Process with FB UI. To the end, FB will response the credential:
   - accessToken
   - data_access_expiration_time
   - userID

5. Get the credential above and input to param of API. like this:

var form = new FormData();
form.append("grant_type", "facebook_login_grant");
form.append("client_id", <CLIENT_ID>);
form.append("client_secret", <CLIENT_SECRET>);
form.append("facebook_access_token", <accessToken>);
form.append("facebook_user_id", <userID>);
form.append("facebook_token_expires_in", <data_access_expiration_time>);

var settings = {
"url": "https://YOUR_DOMAIN/oauth/token",
"method": "POST",
"timeout": 0,
"headers": {
"Cookie": "SSESS88ad8963ae824ba45ea83d243f3c311f=3eU2JqeRxhIb2muHhvlVgIc-eRWc8x-YZSXGIJmzEzs"
},
"processData": false,
"mimeType": "multipart/form-data",
"contentType": false,
"data": form
};

$.ajax(settings).done(function (response) {
    console.log(response);
});


Maintainers
-----------

* Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
