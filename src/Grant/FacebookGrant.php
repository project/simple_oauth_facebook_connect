<?php

namespace Drupal\simple_oauth_facebook_connect\Grant;

use DateInterval;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_facebook\FacebookAuthManager;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Facebook grant class.
 */
class FacebookGrant extends AbstractGrant {

  /**
   * Drupal\social_auth_facebook\FacebookAuthManager definition.
   *
   * @var \Drupal\social_auth_facebook\FacebookAuthManager
   */
  protected $socialAuthFacebookManager;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth user authenticator..
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ModuleHandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    UserRepositoryInterface $userRepository,
    RefreshTokenRepositoryInterface $refreshTokenRepository,
    FacebookAuthManager $socialAuthFacebookManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountProxyInterface $currentUser,
    ModuleHandler $moduleHandler
  ) {
    $this->setUserRepository($userRepository);
    $this->setRefreshTokenRepository($refreshTokenRepository);
    $this->socialAuthFacebookManager = $socialAuthFacebookManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;
    $this->refreshTokenTTL = new DateInterval('P1M');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function respondToAccessTokenRequest(
    ServerRequestInterface $request,
    ResponseTypeInterface $responseType,
    DateInterval $accessTokenTTL
  ) {
    $client = $this->validateClient($request);
    $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
    $user = $this->validateUser($request, $client);

    $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

    $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
    $this->getEmitter()
      ->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
    $responseType->setAccessToken($accessToken);

    $refreshToken = $this->issueRefreshToken($accessToken);

    if ($refreshToken !== NULL) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
      $responseType->setRefreshToken($refreshToken);
    }

    return $responseType;
  }

  /**
   * Validate user by facebook credentials.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   Psr\Http\Message\ServerRequestInterface.
   * @param \League\OAuth2\Server\Entities\ClientEntityInterface $client
   *   League\OAuth2\Server\Entities\ClientEntityInterface.
   *
   * @return \League\OAuth2\Server\Entities\UserEntityInterface
   *   League\OAuth2\Server\Entities\UserEntityInterface.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   */
  protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client) {
    $facebook_access_token = $this->getRequestParameter('facebook_access_token', $request);
    $facebook_token_expires_in = $this->getRequestParameter('facebook_token_expires_in', $request);

    if (empty($facebook_access_token)) {
      throw OAuthServerException::invalidRequest('facebook_access_token');
    }
    if (empty($facebook_token_expires_in)) {
      throw OAuthServerException::invalidRequest('facebook_token_expires_in');
    }
    try {
      $this->socialAuthFacebookManager->setAccessToken(new AccessToken([
        'access_token' => $facebook_access_token,
        'expires_in' => $facebook_token_expires_in,
      ]));
      /* @var \League\OAuth2\Client\Provider\AbstractProvider|false $client */
      $client = $this->networkManager->createInstance('social_auth_facebook')
        ->getSdk();

      $this->socialAuthFacebookManager->setClient($client);
      /** @var \League\OAuth2\Client\Provider\FacebookUser $profile */
      $profile = $this->socialAuthFacebookManager->getUserInfo();
      if (empty($profile)) {
        throw OAuthServerException::invalidCredentials();
      }

      $this->userAuthenticator->setPluginId('social_auth_facebook');
      $data = $this->userAuthenticator->checkProviderIsAssociated($profile->getId()) ? NULL : $this->socialAuthFacebookManager->getExtraDetails();
      $this->userAuthenticator->authenticateUser($profile->getName(),
        $profile->getEmail(),
        $profile->getId(),
        $this->socialAuthFacebookManager->getAccessToken(),
        $profile->getPictureUrl(), $data);
      $this->moduleHandler->invokeAll('simple_oauth_facebook_connect_after_login', [$profile]);
    }
    catch (\Exception $exception) {
      throw new OAuthServerException(
        $exception->getMessage(),
        $exception->getCode(),
        'invalid_facebook_credentials',
        400
      );
    }


    if (!$this->currentUser->isAuthenticated()) {
      throw OAuthServerException::invalidCredentials();
    }
    $user = new UserEntity();
    $user->setIdentifier($this->currentUser->id());
    if ($user instanceof UserEntityInterface === FALSE) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

      throw OAuthServerException::invalidCredentials();
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return 'facebook_login_grant';
  }

}
